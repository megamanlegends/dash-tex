"use strict";

const fs = require("fs");

// Check the number of arguments

if(process.argv.length !== 5) {
	console.error("Usage: node replace.js <rom> <json> <out>");
	process.exit();
}

// Check the rom file

let rom;
let ext = process.argv[2].split(".").pop().toLowerCase();

switch(ext) {
case "img":
case "bin":
	rom = fs.readFileSync(process.argv[2]);
	break;
default:
	console.error("Expecting .bin or .img for rom");
	process.exit();
	break;
}

// Check the json file

ext = process.argv[3].split(".").pop().toLowerCase();
if(ext !== "json") {
	console.error("Expecting .json for patch file");
	process.exit();
}

// Read the json file

let buffer = fs.readFileSync(process.argv[3]);
let json = JSON.parse(buffer.toString());

// Look up file offsets

const FILES = {};

json.files.forEach(file => {

	let name = file.name;
	let first = file.files.shift();
	let queue = file.files;

	let index = 0;
	let found = false;

	do {
		
		// Find index
		index = rom.indexOf(first, index);

		// Throw away if not found
		if(index === -1) {
			console.error("Could not offset for %s", name);
			break;
		}

		let dots = index;
		dots++;
		
		let sum = 0;
		for(let i = 0; i < queue.length; i++) {
			
			dots = rom.indexOf("..\\", dots);
			let str = rom.toString("ascii", dots, dots + 0x20);
			str = str.replace(/\0/g, '');
			
			if(queue[i] !== str) {
				break;
			}
			
			sum++;
			dots++;

		}

		if(sum === queue.length) {
			FILES[name] = index;
			break;
		}

		// Not found search next

		index++;

	} while(index !== -1);

});

// Replace Textures

json.tex.forEach(tim => {

	let name = tim.name;
	let file = tim.file;

	let srcPal = Buffer.from(tim.srcPal, "base64");
	let srcImg = Buffer.from(tim.srcImg, "base64");
	let dstPal = Buffer.from(tim.dstPal, "base64");
	let dstImg = Buffer.from(tim.dstImg, "base64");

	let a = Buffer.allocUnsafe(0x800);
	let b = Buffer.allocUnsafe(0x800);
	const LEN = 0x800;

	if(FILES[file]) {

		// Replace the single file

		let timOfs = rom.indexOf(name, FILES[file]);
		let palOfs = rom.indexOf(srcPal, timOfs);
		dstPal.copy(rom, palOfs);
		
		let count = Math.ceil(srcImg.length / LEN);

		for(let i = 0; i < count; i++) {
			
			let start = i * LEN;
			let end = (i + 1) * LEN;
			if(end > srcImg.length) {
				end = srcImg.length;
			}

			srcImg.copy(a, 0, start, end);
			dstImg.copy(b, 0, start, end);

			timOfs = rom.indexOf(a, timOfs);
			b.copy(rom, timOfs);

			timOfs++;
		}

	} else {

		// Replace all instances

		let index = -1;

		while( (index = rom.indexOf(name, index + 1)) !== -1) {

			// Replace the single file

			let timOfs = rom.indexOf(name, FILES[name]);
			let palOfs = rom.indexOf(srcPal, timOfs);
			dstPal.copy(rom, palOfs);
		
			let count = Math.ceil(srcImg.length / LEN);
			for(let i = 0; i < count; i++) {
			
				let start = i * LEN;
				let end = (i + 1) * LEN;
				if(end > srcImg.length) {
					end = srcImg.length;
				}

				srcImg.copy(a, 0, start, end);
				dstImg.copy(b, 0, start, end);

				timOfs = rom.indexOf(a, timOfs);
				b.copy(rom, timOfs);

				timOfs++;
			}

		}

	}
		
});

fs.writeFileSync(process.argv[4], rom);
