const TextureEditor = (function() {
	
	this.MEM = {
		db : new Dexie("mml1-tex-edit")
	}

	this.DOM = {
		canvas : document.getElementById('TextureEditor.canvas'),
		input : {
			file : document.getElementById("TextureEditor.input.file"),
			image : document.getElementById("TextureEditor.input.image"),
			pallet : document.getElementById("TextureEditor.input.pallet")
		},
		tray : {
			import : document.getElementById("TextureEditor.tray.import"),
			clear : document.getElementById("TextureEditor.tray.clear"),
			export : document.getElementById("TextureEditor.tray.export"),
			list : document.getElementById("TextureEditor.tray.list"),
			gitlab : document.getElementById("TextureEditor.tray.gitlab")
		},
		pallet: {
			active : document.getElementById("TextureEditor.pallet.active"),
			select : document.getElementById("TextureEditor.pallet.select"),
			count : document.getElementById("TextureEditor.pallet.count"),
			colors : document.getElementById("TextureEditor.pallet.colors"),
		},
		set : {
			image : document.getElementById("DashTexture.set.image"),
			pallet : document.getElementById("DashTexture.set.pallet"),
			width : document.getElementById("DashTexture.set.width"),
			height : document.getElementById("DashTexture.set.height"),
			dirty : document.getElementById("DashTexture.set.dirty"),
			restore : document.getElementById("DashTexture.set.restore")
		}
	}

	this.EVT = {	
		handleGitlabClick : evt_handleGitlabClick.bind(this),
		handleExportClick : evt_handleExportClick.bind(this),
		handleFileClick : evt_handleFileClick.bind(this),
		handleFileSelect : evt_handleFileSelect.bind(this),
		handleClearClick : evt_handleClearClick.bind(this),
		handleImageClick : evt_handleImageClick.bind(this),
		handlePalletChange : evt_handlePalletChange.bind(this),
		handleSetImage : evt_handleSetImage.bind(this),
		handleImageSelect : evt_handleImageSelect.bind(this),
		handlePalletClick : evt_handlePalletClick.bind(this),
		handlePalletSelect : evt_handlePalletSelect.bind(this),
		handleRestoreClick : evt_handleImageRestore.bind(this)
	}

	this.API = {
		encodePallet : api_encodePallet.bind(this),
		encodeImage : api_encodeImage.bind(this),
		scanBinFile : api_scanBinFile.bind(this),
		renderTray : api_renderTray.bind(this),
		renderPallet : api_renderPallet.bind(this),
		renderImage : api_renderImage.bind(this)
	}

	init.apply(this);
	return this;

	function init() {

		// Set Canvas Size
		
		this.MEM.ctx = this.DOM.canvas.getContext("2d");

		// Create Database

		this.MEM.db.version(1).stores({
			tim : '&[name+file+index],file,dirty',
			files : '&name'
		});
		
		this.MEM.db.open();

		// Add Events

		this.DOM.tray.import.addEventListener("click", this.EVT.handleFileClick);
		this.DOM.input.file.addEventListener("change", this.EVT.handleFileSelect);
		this.DOM.tray.clear.addEventListener("click", this.EVT.handleClearClick);
		this.DOM.pallet.select.addEventListener("change", this.EVT.handlePalletChange);

		this.DOM.set.image.addEventListener("click", this.EVT.handleSetImage);
		this.DOM.input.image.addEventListener("change", this.EVT.handleImageSelect);

		this.DOM.set.pallet.addEventListener("click", this.EVT.handlePalletClick);
		this.DOM.input.pallet.addEventListener("change", this.EVT.handlePalletSelect);

		this.DOM.set.restore.addEventListener("click", this.EVT.handleRestoreClick);
		this.DOM.tray.gitlab.addEventListener("click", this.EVT.handleGitlabClick);
		this.DOM.tray.export.addEventListener("click", this.EVT.handleExportClick)

		// Render Tray
		
		this.API.renderTray();

	}

	async function evt_handleExportClick() {

		let OUTPUT = {
			files : [],
			tex : []
		};

		let query = { dirty : 1 };
		
		let files = [];
		let tims = await this.MEM.db.tim.where(query).toArray();

		tims.forEach(tim => {

			if(files.indexOf(tim.file) === -1) {
				files.push(tim.file);
			}

			let img = {
				name : tim.name,
				file : tim.file,
				srcImg : "",
				srcPal : "",
				dstImg : "",
				dstPal : ""
			};

			// Source Palette

			tim.pallet.forEach(pal => {
				
				let array = new Uint8Array(pal.buffer);
				let code = String.fromCharCode.apply(null, array)
				img.srcPal += btoa(code);

			});

			// Source Image
				
			img.srcImg = btoa(String.fromCharCode.apply(null, tim.image));
			
			// Update Palette

			tim.updatePallet.forEach(pal => {
				
				let array = new Uint8Array(pal.buffer);
				let code = String.fromCharCode.apply(null, array)
				img.dstPal += btoa(code);

			});

			// Update Image
			
			img.dstImg = btoa(String.fromCharCode.apply(null, tim.updateImage));
			OUTPUT.tex.push(img);

		});
		
		for(let i = 0; i < files.length; i++) {
			let file = await this.MEM.db.files.get({name: files[i]});
			OUTPUT.files.push(file);
		}

		let str = JSON.stringify(OUTPUT);
		var blob = new Blob([str], {type: "text/plain;charset=utf-8"});
		saveAs(blob, "dash_texture.json");

	}

	function evt_handleGitlabClick() {

		const URL = "https://gitlab.com/megamanlegends/dash-tex";
		window.open(URL, '_blank');

	}

	function evt_handleFileClick() {

		this.DOM.input.file.click();

	}

	function evt_handleFileSelect(evt) {

		async.eachSeries(evt.target.files, (file, nextFile) => {
	
			let reader = new FileReader();

			reader.onload = (evt) => {

				let buffer = evt.target.result;
				this.API.scanBinFile(file.name, buffer, nextFile);

			}

			reader.onerror = function(err) {
				throw err;
			}

			reader.readAsArrayBuffer(file);

		}, () => {
			
			this.API.renderTray();

		});

	}

	function evt_handleClearClick() {

		let bool = confirm("Are you sure you want to clear all of the files?");
		if(!bool) {
			return;
		}

		this.MEM.db.tim.clear();
		this.MEM.db.files.clear();

		this.API.renderTray();

	}

	async function evt_handleImageClick(evt) {

		let elm = evt.target;
		
		while(elm.parentNode && elm.tagName !== "LI") {
			elm = elm.parentNode;
		}

		let query = {
			file : elm.getAttribute("data-file"),
			index : parseInt(elm.getAttribute("data-index")),
			name : elm.getAttribute("data-name")
		};

		let tim;
	
		try {
			tim = await this.MEM.db.tim.get(query);
		} catch(err) {
			throw err;
		}

		if(!tim) {
			return console.error("Could not load tim file");
		}
	
		if(this.MEM.li) {
			this.MEM.li.classList.remove("active");
		}

		this.MEM.li = elm;
		this.MEM.li.classList.add("active");

		let suffix = " palette";
		if(tim.nb_pallet > 1) {
			suffix += "s";
		}

		this.DOM.pallet.count.textContent = tim.nb_pallet + suffix;
		this.DOM.pallet.colors.textContent = tim.nb_colors + " colors";
		this.DOM.pallet.select.innerHTML = "";

		if(tim.dirty) {
			this.DOM.set.dirty.textContent = "yes";
		} else {
			this.DOM.set.dirty.textContent = "no";
		}

		for(let i = 0; i < tim.nb_pallet; i++) {
			let opt = document.createElement("option");
			opt.textContent = "Palette " + i;
			this.DOM.pallet.select.appendChild(opt);
		}

		this.DOM.set.width.textContent = tim.width;
		this.DOM.set.height.textContent = tim.height;

		console.log("tim selected");
		console.log("width %d height %d", tim.width, tim.height);

		this.MEM.tim = tim;
		this.API.renderPallet(0);

	}

	function evt_handlePalletChange(evt) {

		let index = this.DOM.pallet.select.selectedIndex;
		this.API.renderPallet(index);

	}

	function evt_handleSetImage() {

		if(!this.MEM.tim) {
			console.log("tim not set");
			return;
		}
		
		console.log("click");
		this.DOM.input.image.click();

	}

	function evt_handleImageSelect(evt) {

		if(!this.MEM.tim) {
			console.log("tim not set");
			return;
		}
		
		if(!evt.target.files.length) {
			console.log("image not set");
			return;
		}

		let reader = new FileReader();

		reader.onload = (evt) => {

			let image = new Image();
			image.src = evt.target.result;
			image.onload = () => {

				if(image.width !== this.MEM.tim.width) {
					console.log("Wrong width");
					return;
				}

				console.log("WIDTH OKAY");

				if(image.height !== this.MEM.tim.height) {
					console.log("Wrong height");
					return;
				}

				console.log("HEIGHT OKAY");
				
				this.API.encodeImage(image);

			}

		}
	
		reader.readAsDataURL(evt.target.files[0])

	}

	function evt_handlePalletClick() {

		if(!this.MEM.tim) {
			console.log("tim not set");
			return;
		}

		console.log("PALLET CLICK!!!");

		this.DOM.input.pallet.click();

	}

	function evt_handlePalletSelect(evt) {
		
		console.log("PALLET CHANGE!!!");

		if(!evt.target.files.length) {
			console.log("image not set");
			return;
		}

		let reader = new FileReader();

		reader.onload = (evt) => {

			let image = new Image();
			image.src = evt.target.result;
			image.onload = () => {

				if(image.width !== this.MEM.tim.nb_colors) {
					console.log("Wrong width");
					return;
				}

				console.log("WIDTH OKAY");

				if(image.height !== 1 && image.height !== this.MEM.tim.nb_pallet) {
					console.log("Wrong height");
					return;
				}

				console.log("HEIGHT OKAY");
				
				this.API.encodePallet(image);

			}

		}
	
		reader.readAsDataURL(evt.target.files[0])

	}

	async function evt_handleImageRestore(evt) {

		if(!this.MEM.tim) {
			return;
		}

		if(!this.MEM.tim.dirty) {
			return;
		}

		let bool = confirm("Would you like to restore this image?");
		if(!bool) {
			return;
		}
		
		this.MEM.tim.dirty = 0;
		delete this.MEM.tim.updatePallet;
		delete this.MEM.tim.updateImage;

		this.API.renderPallet(this.MEM.selectedIndex);

		try {
			await this.MEM.db.tim.put(this.MEM.tim);	
		} catch(err) {
			throw err;
		}

		this.DOM.set.dirty.textContent = "no";
		this.MEM.li.classList.remove("dirty");

	}
		
	async function api_encodePallet(img) {

		let canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;
		let ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		this.MEM.tim.dirty = 1;
		if(!this.MEM.tim.updatePallet) {
			this.MEM.tim.updatePallet = [];
			for(let i = 0; i < this.MEM.tim.pallet.length; i++) {
				this.MEM.tim.updatePallet[i] = this.MEM.tim.pallet[i].slice(0);
			}
		}

		let start = 0;
		if(img.height === 1) {
			start = this.MEM.selectedIndex;
			console.log("SETTING START TO %d", start);
		} 

		let y = 0;
		for(let i = start; i < this.MEM.tim.nb_pallet; i++) {
			
			let palette = [];
			console.log(i);

			for(let x = 0; x < this.MEM.tim.nb_colors; x++) {

				let imgData = ctx.getImageData(x, y, 1, 1);

				console.log(imgData);

				let r = parseInt((imgData.data[0] / 255) * 31);
				let g = parseInt((imgData.data[1] / 255) * 31);
				let b = parseInt((imgData.data[2] / 255) * 31);
				let a = imgData.data[3];

				if(a < 255) {
				    a = 0;
				}

				r = r << 0x00;
				g = g << 0x05;
				b = b << 0x0a;
				a = a << 0x0f;

				let color = r | g | b | a;
				palette[x] = color;
		
			}
			
			this.MEM.tim.updatePallet[i] = new Uint16Array(palette);

			if(img.height === 1) {
				break;
			}

			y++;

		}

		console.log("updating pallet");

		this.API.renderPallet(this.MEM.selectedIndex);

		try {
			await this.MEM.db.tim.put(this.MEM.tim);	
		} catch(err) {
			throw err;
		}

		this.DOM.set.dirty.textContent = "yes";
		this.MEM.li.classList.add("dirty");

	}

	async function api_encodeImage(img) {

		console.log("encoding image");
		
		let canvas = document.createElement("canvas");
		canvas.width = img.width;
		canvas.height = img.height;

		let ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);

		let paletteKeys = {};
		let palette = new Array(this.MEM.tim.nb_colors);

		for(let i = 0; i < palette.length; i++) {
			palette[i] = 0;
		}

		let encodeLength = img.width * img.height / 2;

		let inc = 1;
		let block_width = 64;
		let block_height = 32;
		
		if(this.MEM.tim.nb_colors === 16) {
			block_width *= 2;
			inc *= 2;
			encodeLength /= 2;
		}

		let image = new Array(encodeLength);

		let y, x, by, bx, pos, byte, low, high, index;
		pos = 0;

		for(y = 0; y < img.height; y += block_height) {
		    for(x = 0; x < img.width; x += block_width) {
				for(by = 0; by < block_height; by++) {
				    for(bx = 0; bx < block_width; bx += inc) {

						switch(this.MEM.tim.nb_colors) {
						case 16:
							index = getPixelIndex(y + by, x + bx);
							low = index;
							index = getPixelIndex(y + by, x + bx + 1);
							high = index;
							byte = (high << 4) | low;
							image[pos++] = byte;
							break;
						case 256:
							index = getPixelIndex(y + by, x + bx);
							image[pos++] = index;
							break;
						}
					
					}
			    }
			}
		}

		this.MEM.tim.dirty = 1;
		if(!this.MEM.tim.updatePallet) {

			this.MEM.tim.updatePallet = [];
			for(let i = 0; i < this.MEM.tim.pallet.length; i++) {
				this.MEM.tim.updatePallet[i] = this.MEM.tim.pallet[i].slice(0);
			}

		}

		let pBuff = new Uint16Array(palette);
		let iBuff = new Uint8Array(image);

		this.MEM.tim.updatePallet[this.MEM.selectedIndex] = pBuff;
		this.MEM.tim.updateImage = iBuff;
		this.API.renderPallet(this.MEM.selectedIndex);

		try {
			await this.MEM.db.tim.put(this.MEM.tim);	
		} catch(err) {
			throw err;
		}

		this.DOM.set.dirty.textContent = "yes";
		this.MEM.li.classList.add("dirty");

		function getPixelIndex(y, x) {

			let imgData = ctx.getImageData(x, y, 1, 1);

			let r = parseInt((imgData.data[0] / 255) * 31);
			let g = parseInt((imgData.data[1] / 255) * 31);
			let b = parseInt((imgData.data[2] / 255) * 31);
			let a = imgData.data[3];

			if(a < 255) {
			    a = 0;
			}

			r = r << 0x00;
			g = g << 0x05;
			b = b << 0x0a;
			a = a << 0x0f;

			let color = r | g | b | a;
			let hex = "0x" + color.toString(16);

			if(hex in paletteKeys) {
			    return paletteKeys[hex];
			}

			let index = Object.keys(paletteKeys).length;
			if(index >= palette.length) {
				throw new Error("Image has too many colors, reduce!!");
			}

			paletteKeys[hex] = index;
			palette[index] = color;

			return index;
		}


	}

	async function api_scanBinFile(file, buffer, callback) {

		let ofs = 0;
		let view = new DataView(buffer);
		let len = buffer.byteLength;
			
		let tim_list = [];
		let struct = [];
		let indices = {};

		do {

			let dots = view.getUint16(ofs + 0x40, true);
			if(dots !== 0x2e2e) {
				continue;
			}

			let str = "";
			for(let i = 0x40; i < 0x60; i++) {
				let c = view.getUint8(ofs + i);
				if(c === 0) {
					break;
				}
				str += String.fromCharCode(c);
			}

			if(str.indexOf('\\') === -1) {
				continue;
			}

			struct.push(str);

			let ext = str.split(".").pop();
			if(ext !== "TIM") {
				continue;
			}

			if(!indices[str]) {
				indices[str] = 0;
			}

			let tim = {
				name : str,
				file : file,
				index : indices[str],
				nb_colors : view.getUint32(ofs + 0x14, true),
				nb_pallet : view.getUint32(ofs + 0x18, true),
				width : view.getUint32(ofs + 0x24, true) * 2,
				height : view.getUint32(ofs + 0x28, true),
				dirty : 0
			};

			indices[str]++;
			tim.pallet = new Array(tim.nb_pallet);
			let palOfs = ofs + 0x100;

			for(let i = 0; i < tim.nb_pallet; i++) {
				
				let pallet = [];

				for(let k = 0; k < tim.nb_colors; k++) {
					let color = view.getUint16(palOfs, true);
					palOfs += 0x02;
					pallet.push(color);
				}
				
				tim.pallet[i] = new Uint16Array(pallet);
			}

			let imgOfs = ofs + 0x800;

			let encodedLength = tim.width * tim.height;
			if(tim.nb_color === 0x10) {
				encodedLength = encodedLength/2;
			}

			let data = new Array(encodedLength);
			
			if(tim.nb_colors === 16) {
				tim.width *= 2;
			}
			
			for(let i = 0; i < encodedLength; i++) {
				let byte = view.getUint8(imgOfs + i);
				data[i] = byte;
			}
			
			tim.image = new Uint8Array(data);
			tim_list.push(tim);

		} while( (ofs += 0x400) < len)

		let query = {
			name : file,
			files : struct
		};
		
		await this.MEM.db.files.add(query);

		for(let i = 0; i < tim_list.length; i++) {
			try {
				await this.MEM.db.tim.add(tim_list[i]);
			} catch(err) {
				console.log(err);
			}
		}
				
		callback();

	}

	function api_renderTray() {

		this.DOM.tray.list.innerHTML = "";	

		let label = "";
		this.MEM.db.tim.orderBy('file').each( tim => {

			let tbl, row, cell;

			if(label !== tim.file) {

				label = tim.file;
				let lbl = document.createElement("li");
				lbl.classList.add("label");

				tbl = document.createElement("table");
				row = tbl.insertRow();
				cell = row.insertCell();
				cell.classList.add("label");
				cell.textContent = tim.file;
				cell = row.insertCell();
				cell.classList.add("pal");
				cell.textContent = "Pal";
				cell = row.insertCell();
				cell.classList.add("pal");
				cell.textContent = "Img";
				lbl.appendChild(tbl);
				this.DOM.tray.list.appendChild(lbl);

			}

			let li = document.createElement("li");
			li.setAttribute("data-index", tim.index);
			li.setAttribute("data-file", tim.file);
			li.setAttribute("data-name", tim.name);
			li.classList.add("tim");
			if(tim.dirty) {
				li.classList.add("dirty");
			}

			tbl = document.createElement("table");
			row = tbl.insertRow();
			cell = row.insertCell();
			cell.classList.add("label");
			cell.textContent = tim.name;
			cell = row.insertCell();
			cell.classList.add("pal");
			cell.textContent = tim.nb_pallet;
			cell = row.insertCell();
			cell.classList.add("pal");
			cell.textContent = tim.width ? 1 : 0;
			li.appendChild(tbl);

			this.DOM.tray.list.appendChild(li);
			li.addEventListener("click", this.EVT.handleImageClick);

		});

	}

	function api_renderPallet(index) {
		
		this.MEM.selectedIndex = index;
		this.API.renderImage(index);

		let div = document.createElement("div");
		div.setAttribute("class", "slot");
		div.setAttribute("id", "TextureEditor.pallet.active");
		
		let dirty = this.MEM.tim.dirty;
		let pal = this.MEM.tim.pallet[index];
		if(dirty && this.MEM.tim.updatePallet[index]) {
			pal = this.MEM.tim.updatePallet[index];
		}
		console.log(pal);
		
		for(let i = 0; i < pal.length; i++) {
			
			let color = pal[i];
			let alpha = color >> 0x0f;
			let c = [
				((color >> 0x00) & 0x1f) << 3,
				((color >> 0x05) & 0x1f) << 3,
				((color >> 0x0a) & 0x1f) << 3,
				color === 0 ? 0 : 1
			];
			
			if(c[3] && !alpha) {
				c[3] = 0.9;
			}

			let pix = "rgba(" +c[0]+ "," +c[1]+ "," +c[2]+ "," +c[3]+ ")";
			c[0] = c[0] >> 3;
			c[1] = c[1] >> 3;
			c[2] = c[2] >> 3;
			
			let r = document.createElement("input");
			let g = document.createElement("input");
			let b = document.createElement("input");
			let a = document.createElement("input");
			let rsp = document.createElement("span");
			let gsp = document.createElement("span");
			let bsp = document.createElement("span");
			let asp = document.createElement("span");

			let pack = {
				index : i,
				r : r,
				g : g,
				b : b,
				a : a,
				rsp : rsp,
				gsp : gsp,
				bsp : bsp,
				asp : asp
			};
			
			let callback = evt_handleColorSlide.bind(this, pack);

			let li = document.createElement("li");
			let table = document.createElement("table");
			let row = table.insertRow();

			// Preview cell

			let prev = row.insertCell();
			prev.setAttribute("rowspan", "4");
			prev.classList.add("preview");

			let t = document.createElement("div");
			t.setAttribute("class", "trans");

			color = document.createElement("div");
			color.setAttribute("class", "color");
			color.style.backgroundColor = pix;
			t.appendChild(color);
			prev.appendChild(t);

			// Red Slider

			let cell = row.insertCell();
			cell.textContent = "R";
			cell.classList.add("letter");

			cell = row.insertCell();
			r.setAttribute("type", "range");
			r.setAttribute("min", "0");
			r.setAttribute("max", "31");
			r.setAttribute("step", "1");
			r.setAttribute("value", c[0]);
			r.addEventListener("change", callback);
			r.addEventListener("input", evt_handleColorInput.bind(r, rsp));

			if(!dirty) {
				r.setAttribute("disabled", "disabled");
			}
			cell.appendChild(r);

			c[0] = c[0].toString(16);
			if(c[0].length < 2) {
				c[0] = "0" + c[0];
			}

			let ro = row.insertCell();
			rsp.textContent = "0x" + c[0];
			ro.classList.add("readout");
			ro.appendChild(rsp);

			// Green

			row = table.insertRow();

			cell = row.insertCell();
			cell.textContent = "G";
			cell.classList.add("letter");

			cell = row.insertCell();
			g.setAttribute("type", "range");
			g.setAttribute("min", "0");
			g.setAttribute("max", "31");
			g.setAttribute("step", "1");
			g.setAttribute("value", c[1]);
			g.addEventListener("change", callback);
			g.addEventListener("input", evt_handleColorInput.bind(g, gsp));
			if(!dirty) {
				g.setAttribute("disabled", "disabled");
			}
			cell.appendChild(g);

			c[1] = c[1].toString(16);
			if(c[1].length < 2) {
				c[1] = "0" + c[1];
			}

			let go = row.insertCell();
			gsp.textContent = "0x" + c[1];
			go.classList.add("readout");
			go.appendChild(gsp);

			// Blue

			row = table.insertRow();

			cell = row.insertCell();
			cell.textContent = "B";
			cell.classList.add("letter");

			cell = row.insertCell();
			b.setAttribute("type", "range");
			b.setAttribute("min", "0");
			b.setAttribute("max", "31");
			b.setAttribute("step", "1");
			b.setAttribute("value", c[2]);
			b.addEventListener("change", callback);
			b.addEventListener("input", evt_handleColorInput.bind(b, bsp));
			if(!dirty) {
				b.setAttribute("disabled", "disabled");
			}
			cell.appendChild(b);

			c[2] = c[2].toString(16);
			if(c[2].length < 2) {
				c[2] = "0" + c[2];
			}

			let bo = row.insertCell();
			bsp.textContent = "0x" + c[2];
			bo.classList.add("readout");
			bo.appendChild(bsp);

			// Alpha

			row = table.insertRow();

			cell = row.insertCell();
			cell.textContent = "A";
			cell.classList.add("letter");

			cell = row.insertCell();
			a.setAttribute("type", "range");
			a.setAttribute("min", "0");
			a.setAttribute("max", "1");
			a.setAttribute("step", "1");
			a.setAttribute("value", alpha);
			a.addEventListener("change", callback);
			a.addEventListener("input", evt_handleColorInput.bind(a, asp));
			if(!dirty) {
				a.setAttribute("disabled", "disabled");
			}
			cell.appendChild(a);

			c[3] = alpha.toString(16);
			if(c[3].length < 2) {
				c[3] = "0" + c[3];
			}

			let ao = row.insertCell();
			asp.textContent = "0x" + c[3];
			ao.classList.add("readout");
			ao.appendChild(asp);

			// Add to virtual div

			li.appendChild(table);
			div.appendChild(li);

		}

		let elm = this.DOM.pallet.active;
		elm.parentNode.replaceChild(div, elm);
		this.DOM.pallet.active = div;

	}

	function api_renderImage(index) {
		
		let dirty = this.MEM.tim.dirty;
		let pal = this.MEM.tim.pallet[index];
		if(dirty && this.MEM.tim.updatePallet[index]) {
			pal = this.MEM.tim.updatePallet[index];
		}
		
		let source = this.MEM.tim.image;
		if(dirty) {
			source = this.MEM.tim.updateImage;
		}

		console.log(source);

		let pallet = new Array(this.MEM.tim.pallet[index].length);

		for(let i = 0; i < pallet.length; i++) {
		
			let color = pal[i];
			let alpha = color >> 0x0f;
			let c = [
				((color >> 0x00) & 0x1f) << 3,
				((color >> 0x05) & 0x1f) << 3,
				((color >> 0x0a) & 0x1f) << 3,
				color === 0 ? 0 : 1
			];

			if(c[3] && !alpha) {
				c[3] = 0.9;
			}

			pallet[i] = "rgba(" +c[0]+ "," +c[1]+ "," +c[2]+ "," +c[3]+ ")";

		}

		console.log(this.MEM.tim);

		let inc = 1;
		let block_width = 64;
		let block_height = 32;
		let width = this.MEM.tim.width;
		let height = this.MEM.tim.height;

		if(this.MEM.tim.nb_colors === 16) {
			inc *= 2;
			block_width *= 2;
		}

		this.MEM.ctx.clearRect(0, 0, this.DOM.canvas.width, this.DOM.canvas.height);
		
		console.log(width, height);

		let i = 0;
		for (let y = 0; y < height; y += block_height) {
			
			for (let x = 0; x < width; x += block_width) {
				for (let by = 0; by < block_height; by++) {
					for (let bx = 0; bx < block_width; bx += inc) {
						
						let byte = source[i++];

						switch(this.MEM.tim.nb_colors) {
						case 16:
							
							this.MEM.ctx.fillStyle = pallet[byte & 0xf];
							this.MEM.ctx.fillRect(x + bx, y + by, 1, 1);
							
							this.MEM.ctx.fillStyle = pallet[byte >> 4];
							this.MEM.ctx.fillRect(x + bx + 1, y + by, 1, 1);

							break;
						case 256:
							
							this.MEM.ctx.fillStyle = pallet[byte];
							this.MEM.ctx.fillRect(x + bx, y + by, 1, 1);

							break;
						}

					}
				}
			}
		}
	}

	function evt_handleColorInput(span) {

		let hex = parseInt(this.value).toString(16);
		if(hex.length < 2) {
			hex = "0" + hex;
		}

		span.textContent = "0x" + hex;

	}

	async function evt_handleColorSlide(pack) {

		console.log("SLIDE DETECTED!!");

		let r = pack.r.value;
		let g = pack.g.value;
		let b = pack.b.value;
		let a = pack.a.value;

		r = r << 0x00;
		g = g << 0x05;
		b = b << 0x0a;
		a = a << 0x0f;

		let color = r | g | b | a;
		this.MEM.tim.updatePallet[this.MEM.selectedIndex][pack.index] = color;
		this.API.renderImage(this.MEM.selectedIndex);

		try{
			await this.MEM.db.tim.put(this.MEM.tim);	
		} catch(err) {
			throw err;
		}

	}

}).apply({});
